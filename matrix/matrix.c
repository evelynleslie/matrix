#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>
#include <float.h>
#include <math.h>
#include <string.h>

#include "matrix.h"

#define IDX(x, y, WIDTH) ((x) * (WIDTH) + (y))

static int g_seed = 0;

static ssize_t g_width = 0;
static ssize_t g_height = 0;
static ssize_t g_elements = 0;

static ssize_t g_nthreads = 1;

typedef struct {
	size_t id;
	const float* array;
	float result;
} comp;

typedef struct {
	size_t id;
	float* array;
	float result;
} sqr;

typedef struct {
	size_t id;
	const float* array;
	int result;
	float v;
} freq;

typedef struct {
	size_t id;
	const float* array;
	float* result;
} arr;

typedef struct {
	size_t id;
	float* result;
} matr;

typedef struct {
	size_t id;
	const float* a;
	const float* b;
	float* result;
} matrix;

////////////////////////////////
///     UTILITY FUNCTIONS    ///
////////////////////////////////

/**
 * Returns pseudorandom number determined by the seed.
 */
int fast_rand(void) {

	g_seed = (214013 * g_seed + 2531011);
	return (g_seed >> 16) & 0x7FFF;
}

/**
 * Sets the seed used when generating pseudorandom numbers.
 */
void set_seed(int seed) {

	g_seed = seed;
}

/**
 * Sets the number of threads available.
 */
void set_nthreads(ssize_t count) {

	g_nthreads = count;
}

/**
 * Sets the dimensions of the matrix.
 */
void set_dimensions(ssize_t order) {

	g_width = order;
	g_height = order;

	g_elements = g_width * g_height;
}

/**
 * Displays given matrix.
 */
void display(const float* matrix) {

	for (ssize_t y = 0; y < g_height; y++) {
		for (ssize_t x = 0; x < g_width; x++) {
			if (x > 0) printf(" ");
			printf("%.2f", matrix[y * g_width + x]);
		}
		printf("\n");
	}
}

/**
 * Displays given matrix row.
 */
void display_row(const float* matrix, ssize_t row) {

	for (ssize_t x = 0; x < g_width; x++) {
		if (x > 0) printf(" ");
		printf("%.2f", matrix[row * g_width + x]);
	}
	printf("\n");
}

/**
 * Displays given matrix column.
 */
void display_column(const float* matrix, ssize_t column) {

	for (ssize_t i = 0; i < g_height; i++) {
		printf("%.2f\n", matrix[i * g_width + column]);
	}
}

/**
 * Displays the value stored at the given element index.
 */
void display_element(const float* matrix, ssize_t row, ssize_t column) {

	printf("%.2f\n", matrix[row * g_width + column]);
}

////////////////////////////////
///   MATRIX INITALISATIONS  ///
////////////////////////////////

/**
 * Returns new matrix with all elements set to zero.
 */
float* new_matrix(void) {

	return calloc(g_elements, sizeof(float));
}

/**
 * Returns new identity matrix.
 */

float* identity_matrix(void) {

	float* result = new_matrix();

	/*
		TODO

		1 0 
		0 1
		
	*/
	
	for(unsigned int i = 0; g_elements > i; i++){
		unsigned int j = i / (unsigned int) g_width;
		unsigned int k = i - (j * (unsigned int) g_width);
		if(j == k){
			result[i] = 1;
		}
		else{
			result[i] = 0;
		}
	}	
	return result;
}

/**
 * Returns new matrix with elements generated at random using given seed.
 */

float* random_matrix(int seed) {

	float* matrix = new_matrix();
	set_seed(seed);
	
	for(int i = 0; g_elements > i; i++){
		matrix[i] = fast_rand();
	}
	return matrix;
}

/**
 * Returns new matrix with all elements set to given value.
 */

float* uniform_matrix(float value) {

	/*
		TODO

		     1 1
		1 => 1 1
	*/

	float* result = new_matrix();
	
	for(unsigned int i = 0; g_elements > i; i++){
		result[i] = value;
	}	
	return result;
}

/**
 * Returns new matrix with elements in sequence from given start and step
 */
float* sequence_matrix(float start, float step) {

	/*
		TODO

		       1 2
		1 1 => 3 4
	*/

	float* result = new_matrix();
	
	for (unsigned int i = 0; i < g_elements; i++) {
		result[i] = start;
		start = start + step;
	}	
	return result;
}

////////////////////////////////
///     MATRIX OPERATIONS    ///
////////////////////////////////

/**
 * Returns new matrix with elements cloned from given matrix.
 */

float* cloned(const float* matrix) {

	float* result = new_matrix();	
	for(int i = 0; g_elements > i; i++){
		result[i] = matrix[i];
	}	
	return result;
}

/**
 * Returns new matrix with elements in ascending order.
 */

int compare (const void * a, const void * b)
{
   return ( *(float*)a - *(float*)b );
}

float* sorted(const void* matrix) {

	/*
		TODO

		3 4    1 2
		2 1 => 3 4

	*/

	float* result = cloned(matrix);
	
	qsort(result, g_elements, sizeof(float), compare);
	
	return result;
}

/**
 * Returns new matrix with elements rotated 90 degrees clockwise.
 */

float* rotated(const float* matrix) {

	/*
		TODO

		1 2    3 1
		3 4 => 4 2
	*/

	float* result = new_matrix();
	
	for (unsigned int i = 0; g_elements> i; i++) {
		int j = i / (unsigned int) g_width; //row
		int k = i - (j * (unsigned int) g_width); //column
		result[IDX(k, (g_width - j - 1), g_width)] = matrix[IDX(j, k ,g_width)];
	}	
	return result;
}

/**
 * Returns new matrix with elements ordered in reverse.
 */

float* reversed(const float* matrix) {

	/*
		TODO

		1 2    4 3
		3 4 => 2 1
	*/

	float* result = new_matrix();
	
	for (unsigned int i = 0; g_elements > i; i++) {
		unsigned int j = g_elements - i - 1;
		result[i] = matrix[j];
	}	
	return result;
}

/**
 * Returns new transposed matrix.
 */

float* transposed(const float* matrix) {

	/*
		TODO

		1 2    1 3
		3 4 => 2 4
	*/

	float* result = new_matrix();
	
	for (unsigned int i = 0; g_elements > i; i++) {
		int j = i / (unsigned int) g_width; //row
		int k = i - (j * (unsigned int) g_width); //column
		result[IDX(j, k, g_width)] = matrix[IDX(k, j, g_width)];
	}	
	return result;
}

/**
 * Returns new matrix with scalar added to each element.
 */


float* scalar_add(const float* matrix, float scalar) {

	/*
		TODO

		1 0        2 1
		0 1 + 1 => 1 2

		1 2        5 6
		3 4 + 4 => 7 8
	*/

	float* result = new_matrix();
	
	for(unsigned int i = 0; g_elements > i; i++){
		result[i] = matrix[i] + scalar;
	}	
	return result;
}

/**
 * Returns new matrix with scalar multiplied to each element.
 */

float* scalar_mul(const float* matrix, float scalar) {

	/*
		TODO

		1 0        2 0
		0 1 x 2 => 0 2

		1 2        2 4
		3 4 x 2 => 6 8
	*/

	float* result = new_matrix();
	
	for(unsigned int i = 0; g_elements > i; i++){
		result[i] = matrix[i] * scalar;
	}	
	return result;
}

/**
 * Returns new matrix that is the result of
 * adding the two given matrices together.
 */

void* mat_add_t(void* args) {

	matrix* wargs = (matrix*) args;

	const unsigned int start = wargs->id * (g_elements/ g_nthreads);
	const unsigned int end = wargs->id == g_nthreads - 1 ? g_elements : (wargs->id + 1) * (g_elements/ g_nthreads);

	for (unsigned int i = start; i < end; i++) {
		wargs->result[i] = wargs->a[i] + wargs->b[i];
	}

	return NULL;
}

float* matrix_add(const float* matrix_a, const float* matrix_b) {

	/*
		TODO

		1 0   0 1    1 1
		0 1 + 1 0 => 1 1

		1 2   4 4    5 6
		3 4 + 4 4 => 7 8
	*/

	float* result = new_matrix();
	
	matrix* args = malloc(sizeof(matrix) * g_nthreads);
	for (unsigned int n = 1; n < g_nthreads; n++) {
		for (unsigned int i = 0; i < g_nthreads; i++) {
			args[i] = (matrix) {
				.id = i,
				.a = matrix_a,
				.b = matrix_b,
				.result = result,
			};
		}

		pthread_t thread_ids[g_nthreads];

		for (unsigned int i = 0; i < g_nthreads; i++) {
			pthread_create(thread_ids + i, NULL, mat_add_t, args + i);
		}

		for (unsigned int i = 0; i < g_nthreads; i++) {
			pthread_join(thread_ids[i], NULL);
		}

	}
	
	free(args);
	return result;
}

/**
 * Returns new matrix that is the result of
 * multiplying the two matrices together.
 */

void* mat_mul_t(void* args) {

	matrix* wargs = (matrix*) args;
	

	const unsigned int start = wargs->id * (g_elements/ g_nthreads);
	const unsigned int end = wargs->id == g_nthreads - 1 ? g_elements : (wargs->id + 1) * (g_elements/ g_nthreads);

	for (unsigned int i = start; i < end; i++) {
		double mul = 0;
		int j = i / (unsigned int) g_width; //row
		int k = i - (j * (unsigned int) g_width); //column
		for (unsigned int l = 0; l < g_width; l++) {
			mul += wargs->a[IDX(j, l, g_width)] * wargs->b[IDX(l, k, g_width)];
		}
		wargs->result[i] = mul;
	}

	return NULL;
}

float* matrix_mul(const float* matrix_a, const float* matrix_b) {

	/*
		TODO

		1 2   1 0    1 2
		3 4 x 0 1 => 3 4

		1 2   5 6    19 22
		3 4 x 7 8 => 43 50
	*/

	float* result = new_matrix();

	matrix* args = malloc(sizeof(matrix) * g_nthreads);
	for (unsigned int n = 1; n < g_nthreads; n++) {
		for (unsigned int i = 0; i < g_nthreads; i++) {
			args[i] = (matrix) {
				.id = i,
				.a = matrix_a,
				.b = matrix_b,
				.result = result,
			};
		}

		pthread_t thread_ids[g_nthreads];

		for (unsigned int i = 0; i < g_nthreads; i++) {
			pthread_create(thread_ids + i, NULL, mat_mul_t, args + i);
		}

		for (unsigned int i = 0; i < g_nthreads; i++) {
			pthread_join(thread_ids[i], NULL);
		}

	}
	
	free(args);
	return result;
}

/**
 * Returns new matrix that is the result of
 * powering the given matrix to the exponent.
 */
float* matrix_pow(const float* matrix, float exponent) {

	/*
		TODO

		1 2        1 0
		3 4 ^ 0 => 0 1

		1 2        1 2
		3 4 ^ 1 => 3 4

		1 2        199 290
		3 4 ^ 4 => 435 634
	*/
	
	float* result = NULL;
	
	if(exponent < 2){
		if(exponent == 1){
			result = cloned(matrix);
		}
		else if (exponent == 0){
			result = identity_matrix();
		}
		else if(exponent == -1){
			if(g_elements == 4 && get_determinant(matrix) != 0){
				result = new_matrix();
				result[0] = matrix[3] * (1/get_determinant(matrix));
				result[1] = matrix[1] * (1/get_determinant(matrix)) * -1;
				result[2] = matrix[2] * (1/get_determinant(matrix)) * -1;
				result[3] = matrix[0] * (1/get_determinant(matrix));
			}
		}
	}
	else{
		result = matrix_mul(matrix, matrix);
		int i = 1;
	
		while(i < exponent - 1){
			float* temp = matrix_mul(result, matrix);
			free(result);
			result = temp;
			i++;
		}
	}

	return result;
}

/**
 * Returns new matrix that is the result of
 * convolving given matrix with a 3x3 kernel matrix.
 */

float conv_temp(float* temp, int k, int l, const float* kernel){

	/*
		TODO

		Convolution is the process in which the values of a matrix are
		computed according to the weighted sum of each value and it's
		neighbours, where the weights are given by the kernel matrix.
	*/

	unsigned int x = 0;
	float wsum = 0;
	for(int i = -1; i <= 1; i++){ /* both grids are the size of [w+2][h+2] for this loop to work */
		for(int j = -1; j <= 1; j++){
			wsum += temp[IDX(k+i, l+j, g_width+2)]*kernel[x];
			x++;
		}
	}
	return wsum;
}

float* matrix_conv(const float* matrix, const float* kernel) {

	float* temp = (float*)malloc(sizeof(float)*((g_width+2)*(g_height+2)));
	float* result = new_matrix();							 
	
	for(unsigned int i = 0; (g_width+2)*(g_height+2) > i; i++){
		unsigned int j = i / (unsigned int) (g_width+2);
		unsigned int k = i - (j * (unsigned int) (g_width+2));

		if(j < g_height && k < g_width){
			temp[IDX(j+1, k+1, g_width+2)] = matrix[IDX(j, k, g_width)];
		}
		if(k == 0 && j == 0){
				temp[i] = matrix[0];
			}
		else if(j == 0 && k == g_width+1){
			temp[i] = matrix[g_width-1];
		}
		else if(k == 0 && j == g_width+1){
			temp[i] = matrix[IDX(j-2, k, g_width)];
		}
		else if(j == g_width+1 && k == g_width+1){
			temp[i] = matrix[IDX(j-2, k-2, g_width)];
		}
			else if(j == 0){
				temp[i] = matrix[IDX(j, k-1, g_width)];
				
			}
			else if(j == g_height+1){
				temp[i] = matrix[IDX(j-2, k-1, g_width)];
			}
			else if(k == 0){
				temp[i] = matrix[IDX(j-1, k, g_width)];
			}
			else if(k == g_width+1){
				temp[i] = matrix[IDX(j-1, k-2, g_width)];
			}
		
	}
	
	for(unsigned int i = 0; g_elements > i; i++){
		unsigned int j = i / (unsigned int) g_width;
		unsigned int k = i - (j * (unsigned int) g_width);
		result[i] = conv_temp(temp, j+1, k+1, kernel);
	}

	free(temp);							 
	return result;
}

////////////////////////////////
///       COMPUTATIONS       ///
////////////////////////////////

/**
 * Returns the sum of all elements.
 */
float get_sum(const float* matrix) {
	
	/*
		TODO

		2 1
		1 2 => 6

		1 1
		1 1 => 4
	*/	

	float sum = 0;
	
	for(unsigned int i = 0; g_elements > i; i++){
		sum += matrix[i];
	}
	
	return sum;
}

/**
 * Returns the trace of the matrix.
 */

float get_trace(const float* matrix) {
	
	/*
		TODO

		1 0
		0 1 => 2

		2 1
		1 2 => 4
	*/

	float trace = 0;
	
	for(unsigned int i = 0; g_elements > i; i++){
		unsigned int j = i / (unsigned int) g_width;
		unsigned int k = i - (j * (unsigned int) g_width);
		if(j == k){
			trace += matrix[i];
		}
	}
	
	return trace;
}

/**
 * Returns the smallest value in the matrix.
 */

void* min_t(void* args) {

	comp* wargs = (comp*) args;

	const unsigned int start = wargs->id * (g_elements/ g_nthreads);
	const unsigned int end = wargs->id == g_nthreads - 1 ? g_elements : (wargs->id + 1) * (g_elements/ g_nthreads);

	double min = wargs->array[0];

	for (unsigned int i = start; i < end; i++) {
		if(wargs->array[i] < min){
			min = wargs->array[i];
		}
	}

	wargs->result = min;
	return NULL;
}

float get_minimum(const float* matrix) {

	/*
		TODO

		1 2
		3 4 => 1

		4 3
		2 1 => 1
	*/

	float min = matrix[0];
	
	comp* args = malloc(sizeof(comp) * g_nthreads);
	for (unsigned int n = 1; n < g_nthreads; n++) {
		for (unsigned int i = 0; i < g_nthreads; i++) {
			args[i] = (comp) {
				.id     = i,
				.array  = matrix,
				.result = 0,
			};
		}

		pthread_t thread_ids[g_nthreads];

		for (unsigned int i = 0; i < g_nthreads; i++) {
			pthread_create(thread_ids + i, NULL, min_t, args + i);
		}

		for (unsigned int i = 0; i < g_nthreads; i++) {
			pthread_join(thread_ids[i], NULL);
		}

		for (unsigned int i = 0; i < g_nthreads; i++) {
			if(args[i].result < min){
				min = args[i].result;
			}
		}
	}
	
	free(args);	
	return min;
}

/**
 * Returns the largest value in the matrix.
 */

void* max_t(void* args) {
	comp* wargs = (comp*) args;
	const unsigned int start = wargs->id * (g_elements/ g_nthreads);
	const unsigned int end = wargs->id == g_nthreads - 1 ? g_elements : (wargs->id + 1) * (g_elements/ g_nthreads);
	double max = wargs->array[0];
	for (unsigned int i = start; i < end; i++) {
		if(wargs->array[i] > max){
			max = wargs->array[i];
		}
	}
	wargs->result = max;
	return NULL;
}

float get_maximum(const float* matrix) {	


	/*
		TODO

		1 2
		3 4 => 4

		4 3
		2 1 => 4
	*/

	float max = matrix[0];

	comp* args = malloc(sizeof(comp) * g_nthreads);
	for (unsigned int n = 1; n < g_nthreads; n++) {
		for (unsigned int i = 0; i < g_nthreads; i++) {
			args[i] = (comp) {
				.id     = i,
				.array  = matrix,
				.result = 0,
			};
		}
		pthread_t thread_ids[g_nthreads];
		for (unsigned int i = 0; i < g_nthreads; i++) {
			pthread_create(thread_ids + i, NULL, max_t, args + i);
		}
		for (unsigned int i = 0; i < g_nthreads; i++) {
			pthread_join(thread_ids[i], NULL);
		}
		for (unsigned int i = 0; i < g_nthreads; i++) {
			if(args[i].result > max){
				max = args[i].result;
			}
		}
	}	
	free(args);
	return max;
}

/**
 * Returns the determinant of the matrix.
 */

float det_temp_call(float* matrix, ssize_t n){	

	float det = 0;
	for(int i = 0; n > i; i++){
		if(i%2 == 0){			
			det += (-1*matrix[i]) * det_temp(matrix, n, i);
		}
		else{
		det += matrix[i] * det_temp(matrix, n, i);
		}
	}	
	return det;
}

float det_temp(float* matrix, ssize_t n, int i){

	int x = 0;
	float det = 0;
	float* temp = (float*)malloc(sizeof(float)*((n-1)*(n-1)));	
	if(n == 2){
		det = (matrix[0]*matrix[3]) - (matrix[1]*matrix[2]);
	}
	else{			
		ssize_t l = i / n;			
		for(ssize_t j = 0; n > j; j++){				
			if(j != l){
				for(ssize_t k = 0; n > k; k++){					
					if(k != i){		
						temp[x] = matrix[n*j+k];						
						x++;
					}
				}
			}
		}	
		if(n-1 == 2){
			det = det_temp(temp, n-1, i);
		}
		else{
			det = det_temp_call(temp, n-1);
		}		
	}
	free(temp);
	return det;
}

float get_determinant(const float* matrix) {

	/*
		TODO

		1 0
		0 1 => 1

		1 2
		3 4 => -2

		8 0 2
		0 4 0
		2 0 8 => 240
	*/
	
	float det = 0;
	
	if(g_width == 1){
		det = matrix[0];
	}
	
	else if(g_width == 2){
		det = (matrix[0]*matrix[3]) - (matrix[1]*matrix[2]);
	}
	
	else{
		float* result = cloned(matrix);
		det = det_temp_call(result, g_width);
		free(result);
	}

	return det;
}
/**
 * Returns the frequency of the given value in the matrix.
 */
ssize_t get_frequency(const float* matrix, float value) {
	
	/*
		TODO

		1 1
		1 1 :: 1 => 4

		1 0
		0 1 :: 2 => 0
	*/

	int f = 0;	

	for(unsigned int i = 0; g_elements > i; i++){
		if(matrix[i] == value){
			f++;
		}
	}	
	return f;
}